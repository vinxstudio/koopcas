<?php

namespace App\Exports;

use App\PlayToSave;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PlayToSave6WinnersExportDate implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $selectedDate;

    public function __construct(string $selectedDate) 
    {
        $this->selectedDate = $selectedDate;
    }

    public function collection()
    {
        return PlayToSave::getAllWinnersForSixDigitDate($this->selectedDate);
    }

    public function headings(): array
    {
        return [
            'Branch',
            'Serial Number',
            'Client ID',
            'First Name',
            'Last Name',
            'Middle Name',
            'Suffix',
            'Winning Number',
            'Prize Win',
            'Date Of Winning'
        ];
    }
}
