<?php

namespace App\Exports;

use App\PlayToSave;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PlayToSave3WinnersExportBranch implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $selectedBranch;

    public function __construct(string $selectedBranch) 
    {
        $this->selectedBranch = $selectedBranch;
    }

    public function collection()
    {
        return PlayToSave::getAllWinnersForThreeDigitBranch($this->selectedBranch);
    }

    public function headings(): array
    {
        return [
            'Branch',
            'Serial Number',
            'Client ID',
            'First Name',
            'Last Name',
            'Middle Name',
            'Suffix',
            'Three Digit Number',
            'Winning Number',
            'Solo/Rumble',
            'Prize Win',
            'Date Of Winning'
        ];
    }
}
