<?php

namespace App\Exports;

use App\PlayToSave;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PlayToSave6WinnersExportBranch implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $selectedBranch;

    public function __construct(string $selectedBranch) 
    {
        $this->selectedBranch = $selectedBranch;
    }

    public function collection()
    {
        return PlayToSave::getAllWinnersForSixDigitBranch($this->selectedBranch);
    }

    public function headings(): array
    {
        return [
            'Branch',
            'Serial Number',
            'Client ID',
            'First Name',
            'Last Name',
            'Middle Name',
            'Suffix',
            'Winning Number',
            'Prize Win',
            'Date Of Winning'
        ];
    }
}
