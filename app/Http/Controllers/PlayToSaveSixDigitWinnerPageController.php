<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlayToSaveSixDigitWinnerPage;
use App\Exports\PlayToSave6WinnersExportBranch;
use App\Exports\PlayToSave6WinnersExportDate;
use Maatwebsite\Excel\Facades\Excel;

class PlayToSaveSixDigitWinnerPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('play-to-save.playtosave-sixdigit-winner.playtosavesixdigitwinner');
    }

    public function downloadWinners6Date($selectedDate)
    {
        return Excel::download(new PlayToSave6WinnersExportDate($selectedDate), 'playtosavecombo6winnersbydate.xlsx');
    }
    public function downloadWinners6Branch($selectedBranch)
    {
        return Excel::download(new PlayToSave6WinnersExportBranch($selectedBranch), 'playtosavecombo6winnersbybranch.xlsx');
    }
}
