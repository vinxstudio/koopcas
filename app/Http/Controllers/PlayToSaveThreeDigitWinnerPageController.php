<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlayToSaveThreeDigitWinnerPage;
use App\Exports\PlayToSave3WinnersExportDate;
use App\Exports\PlayToSave3WinnersExportBranch;
use Maatwebsite\Excel\Facades\Excel;

class PlayToSaveThreeDigitWinnerPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('play-to-save.playtosave-threedigit-winner.playtosavethreedigitwinner');
    }

    public function downloadWinners3Date($selectedDate)
    {
        return Excel::download(new PlayToSave3WinnersExportDate($selectedDate), 'playtosavecombo3winnersbydate.xlsx');
    }

    public function downloadWinners3Branch($selectedBranch)
    {
        return Excel::download(new PlayToSave3WinnersExportBranch($selectedBranch), 'playtosavecombo3winnersbybranch.xlsx');
    }
}
