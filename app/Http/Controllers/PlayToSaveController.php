<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlayToSave;
use App\Http\Resources\PlayToSaveSixDigitResource;
use App\Http\Resources\PlayToSaveThreeDigitResource;
use App\Http\Resources\PlayToSaveSixDigitNumWinResource;
use App\Http\Resources\PlayToSaveThreeDigitNumWinResource;
use App\Http\Resources\PlayToSaveSixDigitEntries;
use App\Http\Resources\PlayToSaveThreeDigitEntries;
use Carbon\Carbon;

class PlayToSaveController extends Controller
{
    /** SIX DIGIT METHODS */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($term)
    {
        $playsix = PlayToSave::getSixDigitNumbers($term);
        return PlayToSaveSixDigitResource::collection($playsix);
    }

    /**
     * Display the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($ptsDnId)
    {
        $playsix = PlayToSave::getSixDigit($ptsDnId);
        return PlayToSaveSixDigitResource::collection($playsix);
    }

    /**
     * Display all resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAll($sixdigit, $dateofdraw)
    {
        $sixdigitentry = PlayToSave::getAllSixDigitEntryWinners($sixdigit, $dateofdraw);
        return PlayToSaveSixDigitEntries::collection($sixdigitentry);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sixdigit = new PlayToSave;

        $sixdigit->PTSSDNumDOC_NO = $request->input('ptsSdNumDocNo');
        $sixdigit->TellerID = $request->input('tellerId');
        $sixdigit->SixDigitNumber = $request->input('sixDigitNumber');
        $sixdigit->SixDigitNumber2 = $request->input('sixDigitNumber2');
        $sixdigit->SixDigitWinPrize = $request->input('sixDigitWinPrize');
        $sixdigit->SDDateOfDraw = $request->input('sdDateOfDraw');
        $sixdigit->SDTotalCollection = $request->input('sdTotalCollection');
        $sixdigit->SDDateTimeOfTrans = $request->input('sdDateTimeTrans');

        PlayToSave::insertSixDigit($sixdigit->PTSSDNumDOC_NO, $sixdigit->TellerID, $sixdigit->SixDigitNumber, $sixdigit->SixDigitNumber2, $sixdigit->SixDigitWinPrize, $sixdigit->SDDateOfDraw,
        $sixdigit->SDTotalCollection, $sixdigit->SDDateTimeOfTrans);
    }

    /** THREE DIGIT METHODS */
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexThree($term)
    {
        $playthree = PlayToSave::getThreeDigitNumbers($term);
        return PlayToSaveThreeDigitResource::collection($playthree);
    }

    /**
     * Display all resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAllThree($threedigit, $dateofdraw, $numdoc)
    {
        $threedigitentry = PlayToSave::getAllThreeDigitEntryWinners($threedigit, $dateofdraw, $numdoc);
        return PlayToSaveThreeDigitEntries::collection($threedigitentry);
    }

    /**
     * Display the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showThree($ptsDnId)
    {
        $playthree = PlayToSave::getThreeDigit($ptsDnId);
        return PlayToSaveThreeDigitResource::collection($playthree);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeThree(Request $request)
    {
        $threedigit = new PlayToSave;

        $threedigit->PTSTDNumDOC_NO = $request->input('ptsTdNumDocNo');
        $threedigit->TellerID = $request->input('tellerId');
        $threedigit->ThreeDigitNum = $request->input('threeDigitNum');
        $threedigit->ThreeDigitWinPrizeSolo = $request->input('threeDigitWinPrizeSolo');
        $threedigit->threeDigitWinPrizeRumble = $request->input('threeDigitWinPrizeRumble');
        $threedigit->TDDateOfDraw = $request->input('tdDateOfDraw');
        $threedigit->TDTotalCollection = $request->input('tdTotalCollection');
        $threedigit->TDDateTimeTrans = $request->input('tdDateTimeTrans');

        PlayToSave::insertThreeDigit($threedigit->PTSTDNumDOC_NO, $threedigit->TellerID, $threedigit->ThreeDigitNum, $threedigit->ThreeDigitWinPrizeSolo,
        $threedigit->threeDigitWinPrizeRumble, $threedigit->TDDateOfDraw, $threedigit->TDTotalCollection, $threedigit->TDDateTimeTrans);
    }

    /** SIX DIGIT WINNER METHODS */
    /**
     * Display the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sixDigitWinners($ptsDnId)
    {
        $sixdigiwinners = PlayToSave::getSixDigitWinners($ptsDnId);
        return PlayToSaveSixDigitNumWinResource::collection($sixdigiwinners);
    }

    public function storeSixDigitWinners(Request $request)
    {
        $sixdigitwinners = new PlayToSave;

        $sixdigitwinners->PTSDWinBR_CODE = $request->input('ptsSdWinBrCode');
        $sixdigitwinners->PTSSDID = $request->input('ptsSdId');
        $sixdigitwinners->DocNumber = $request->input('docNumber');
        $sixdigitwinners->TellerID = $request->input('tellerId');
        $sixdigitwinners->ClientAcctNum = $request->input('clientAcctNum');
        $sixdigitwinners->ClientName = $request->input('clientName');
        $sixdigitwinners->WinningNumber = $request->input('winningNumber');
        $sixdigitwinners->PrizeWon = $request->input('prizeWon');
        $sixdigitwinners->DateOfWinning = $request->input('dateOfWinning');
        $sixdigitwinners->DateTimeOfTran = $request->input('dateTimeOfTrans');

        PlayToSave::insertSixDigitWinners($sixdigitwinners->PTSDWinBR_CODE, $sixdigitwinners->PTSSDID, $sixdigitwinners->DocNumber, $sixdigitwinners->TellerID, $sixdigitwinners->ClientAcctNum, 
        $sixdigitwinners->ClientName, $sixdigitwinners->WinningNumber, $sixdigitwinners->PrizeWon, $sixdigitwinners->DateOfWinning, $sixdigitwinners->DateTimeOfTran);
    }

    /** THREE DIGIT WINNER METHODS */
    /**
     * Display the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function threeDigitWinners($threedigit, $date)
    {
        $threedigitwinners = PlayToSave::getThreeDigitWinners($threedigit);
        return PlayToSaveThreeDigitNumWinResource::collection($threedigitwinners);
    }

    public function storeThreeDigitWinners(Request $request)
    {
        $threedigitwinners = new PlayToSave;
        $threedigitwins = [];

        $threedigitwinners->PTSTDWinBR_CODE = $request->input('ptsTdWinBrCode');
        $threedigitwinners->PTSTDID = $request->input('ptsTdId');
        $threedigitwinners->ClientAcctNum = $request->input('clientAcctNum');
        $threedigitwinners->ClientName = $request->input('clientName');
        $threedigitwinners->TellerID = $request->input('tellerId');
        $threedigitwinners->WinningNumber = $request->input('winningNumber');
        $threedigitwinners->PrizeWon = $request->input('prizeWon');
        $threedigitwinners->DateOfWinning = $request->input('dateOfWinning');
        $threedigitwinners->DateTimeOfTrans = $request->input('dateTimeOfTrans');
        $threedigitwinners->PTSSerialID = $request->input('ptsSerialId');
        $threedigitwinners->PTSStabsNumber = $request->input('ptsStabsNumber');
        $threedigitwinners->PTSTDAdvance = $request->input('ptsTdAdvance');
        $threedigitwinners->PTSTDNumOfEntries = $request->input('ptsTdNumOfEntries');
        $threedigitwinners->PTSTDRumbleSolo = $request->input('ptsTdRumbleSolo');
        $threedigitwinners->PTSTDTRDOC_NO = $request->input('ptsTdTrDocNo');
        $threedigitwinners->PTSThreeDigitNum = $request->input('ptsThreeDigitNum');

        PlayToSave::insertThreeDigitWinners(
            $threedigitwinners->PTSTDWinBR_CODE,
            $threedigitwinners->PTSTDID,
            $threedigitwinners->ClientAcctNum,
            $threedigitwinners->ClientName,
            $threedigitwinners->TellerID,
            $threedigitwinners->WinningNumber,
            $threedigitwinners->PrizeWon,
            $threedigitwinners->DateOfWinning,
            $threedigitwinners->DateTimeOfTrans,
            $threedigitwinners->PTSSerialID,
            $threedigitwinners->PTSStabsNumber,
            $threedigitwinners->PTSTDAdvance,
            $threedigitwinners->PTSTDNumOfEntries,
            $threedigitwinners->PTSTDRumbleSolo,
            $threedigitwinners->PTSTDTRDOC_NO,
            $threedigitwinners->PTSThreeDigitNum);
    }

    /** SIX DIGIT ENTRY METHODS */
    /**
     * Display the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSixDigitEntryById($clientId)
    {
        $sixdigitentry = PlayToSave::getSixDigitEntry($clientId);
        return PlayToSaveSixDigitEntries::collection($sixdigitentry);
    }

    public function getSixDigitById($serialId, $branch, $serialNum)
    {
        $sixdigitnum = PlayToSave::getSixDigitByTicket($serialId, $branch, $serialNum);
        return PlayToSaveSixDigitEntries::collection($sixdigitnum);
    }

    public function insertSixDigitEntry(Request $request)
    {
        $sixdigitentry = new PlayToSave;

        $year = date('Y'); $month = date('m'); $day = date('d');
        $hour = 12; $minute = 00; $seconds = 00; $tz = 'Asia/Manila';
        $drawhr = 05; $drawmin = 00; $drawsec = 00;
        $dateNowCreated = Carbon::create($year, $month, $day, $hour, $minute, $seconds, $tz)->format('Y-m-d h:i a');
        $dateTomorowCreated = Carbon::create($year, $month, $day, $hour, $minute, $seconds, $tz)->addHours(24)->format('Y-m-d h:i a');

        $sixdigitentry->PTSSDTR_ID = $request->input('ptsTrId');
        $sixdigitentry->PTSSDEBR_CODE = $request->input('ptsSdeBrCode');
        $sixdigitentry->TellerID = $request->input('tellerId');
        $sixdigitentry->PTSSerialID = $request->input('ptsSerialId');
        $sixdigitentry->PTSStabsNumber = $request->input('ptsStabsNumber');
        $sixdigitentry->PTSSDClientID = $request->input('ptsSdClientId');
        $sixdigitentry->PTSSDTRDOC_NO = Carbon::today('Asia/Manila')->format('Ymd');
        $sixdigitentry->PTSSDNumOfEntries = $request->input('ptsSdNumOfEntries');
        $sixdigitentry->PTSSDDateOfTrans = Carbon::today('Asia/Manila')->format('Y-m-d');
        $sixdigitentry->PTSSixDigitNum = $request->input('ptsSixDigitNum');
        $sixdigitentry->PTSSixDigitNum2 = $request->input('ptsSixDigitNum2');
        $sixdigitentry->PTSSDDateTrans = Carbon::today('Asia/Manila')->format('Y-m-d');

        if (Carbon::now() <= $dateNowCreated && $dateTomorowCreated >= $dateNowCreated) {
            $sixdigitentry->PTSSDDateOfDraw = Carbon::create($year, $month, $day, $drawhr, $drawsec, $drawmin)->addHours(36)->format('Y-m-d h:i:s');
        }

        $sixdigitentry->PTSSDAdvance = $request->input('ptsSdAdvance');
        $sixdigitentry->PTSSDDateTime = Carbon::now('Asia/Manila')->format('Y-m-d h:i:s');

        PlayToSave::insertSixDigitEntry($sixdigitentry->PTSSDTR_ID, $sixdigitentry->PTSSDEBR_CODE, $sixdigitentry->TellerID, $sixdigitentry->PTSSerialID, $sixdigitentry->PTSStabsNumber, $sixdigitentry->PTSSDClientID, $sixdigitentry->PTSSDTRDOC_NO, $sixdigitentry->PTSSDNumOfEntries,
        $sixdigitentry->PTSSDDateOfTrans, $sixdigitentry->PTSSixDigitNum, $sixdigitentry->PTSSixDigitNum2, $sixdigitentry->PTSSDDateTrans, $sixdigitentry->PTSSDDateOfDraw, $sixdigitentry->PTSSDAdvance, $sixdigitentry->PTSSDDateTime);
    }

    /** THREE DIGIT ENTRY METHODS */
     /**
     * Display the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getThreeDigitEntryById($clientId)
    {
        $threedigitentry = PlayToSave::getThreeDigitEntry($clientId);
        return PlayToSaveThreeDigitEntries::collection($threedigitentry);
    }

    public function getThreeDigitById($serialId, $branch, $serialNum)
    {
        $threedigitnum = PlayToSave::getThreeDigitById($serialId, $branch, $serialNum);
        return PlayToSaveThreeDigitEntries::collection($threedigitnum);
    }

    public function insertThreedigitEntry(Request $request)
    {
        $threedigitentry = new PlayToSave;

        $year = date('Y'); $month = date('m'); $day = date('d');
        $hour = 12; $minute = 00; $seconds = 00; $tz = 'Asia/Manila';
        $drawhr = 05; $drawmin = 00; $drawsec = 00;
        $dateNowCreated = Carbon::create($year, $month, $day, $hour, $minute, $seconds, $tz)->format('Y-m-d h:i a');
        $dateTomorowCreated = Carbon::create($year, $month, $day, $hour, $minute, $seconds, $tz)->addHours(24)->format('Y-m-d h:i a');

        $threedigitentry->PTSTDTR_ID = $request->input('ptsTrId');
        $threedigitentry->PTSTDEBR_CODE = $request->input('ptsTdeBrCode');
        $threedigitentry->TellerID = $request->input('tellerId');
        $threedigitentry->PTSSerialID = $request->input('ptsSerialId');
        $threedigitentry->PTSStabsNumber = $request->input('ptsStabsNumber');
        $threedigitentry->PTSTDClientID = $request->input('ptsTdClientId');
        $threedigitentry->PTSTDTRDOC_NO = Carbon::today('Asia/Manila')->format('Ymd');
        $threedigitentry->PTSTDNumOfEntries = $request->input('ptsTdNumOfEntries');
        $threedigitentry->PTSTDDateOfTrans = Carbon::today('Asia/Manila')->format('Y-m-d');
        $threedigitentry->PTSThreeDigitNum = $request->input('ptsThreeDigitNum');
        $threedigitentry->PTSTDRumbleSolo = $request->input('ptsTdRumbleSolo');
        $threedigitentry->PTSTDDateTrans = Carbon::today('Asia/Manila')->format('Y-m-d');

        if (Carbon::now() <= $dateNowCreated && $dateTomorowCreated >= $dateNowCreated) {
            $threedigitentry->PTSTDDateOfDraw = Carbon::create($year, $month, $day, $drawhr, $drawsec, $drawmin)->addHours(36)->format('Y-m-d h:i:s');
        }

        $threedigitentry->PTSTDAdvance = $request->input('ptsTdAdvance');
        $threedigitentry->PTSTDDateTime = Carbon::now('Asia/Manila')->format('Y-m-d h:i:s');

        PlayToSave::insertThreeDigitEntry($threedigitentry->PTSTDTR_ID, $threedigitentry->PTSTDEBR_CODE, $threedigitentry->TellerID, $threedigitentry->PTSSerialID, $threedigitentry->PTSStabsNumber, $threedigitentry->PTSTDClientID, $threedigitentry->PTSTDTRDOC_NO, $threedigitentry->PTSTDNumOfEntries,
        $threedigitentry->PTSTDDateOfTrans, $threedigitentry->PTSThreeDigitNum, $threedigitentry->PTSTDRumbleSolo, $threedigitentry->PTSTDDateTrans, $threedigitentry->PTSTDDateOfDraw, $threedigitentry->PTSTDAdvance, $threedigitentry->PTSTDDateTime);
    }
}
