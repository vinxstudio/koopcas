<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PlayToSaveThreeDigitNumWinResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'ptsTdWinId' => $this->PTSTDWinID,
            'ptsTdWinBrCode' => $this->PTSTDWinBR_CODE,
            'ptsTdId' => $this->PTSTDID,
            'tellerId' => $this->TellerID,
            'serialNumber' => $this->SerialNumber,
            'threeDigitNum' => $this->ThreeDigitNum,
            'threeDigitWinPrizeSolo' => $this->ThreeDigitWinPrizeSolo,
            'threeDigitWinPrizeRumble' => $this->ThreeDigitWinPrizeRumble,
            'clientIdNum' => $this->ClientIDNum,
            'winningNumber' => $this->WinningNumber,
            'ticketType' => $this->TicketType,
            'prizeWon' => $this->PrizeWon,
            'dateOfWinning' => $this->DateOfWinning,
            'dateTimeOfTrans' => $this->DateTimeOfTrans,
        ];
    }
}
