<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class PlayToSave extends Model
{
    /** SIX DIGIT QUERIES */
    public static function getSixDigitNumbers($term)
    {
        return DB::table('p2ssixdigitnum')
                    ->where('SDDateOfDraw', '=', $term)
                    ->orWhere('PTSSDNumDOC_NO', '=', $term)
                    ->get();
    }

    public static function getSixDigit($ptsSdId)
    {
        return DB::table('p2ssixdigitnum')
                    ->where('PTSSDID', '=', $ptsSdId)
                    ->get();
    }

    public static function insertSixDigit($ptsSdNumDocNo, $tellerId, $sixDigitNumber, $sixDigitNumber2, $sixDigitWinPrize, $sdDateOfDraw, $sdTotalCollection, $sdDateTimeTrans)
    {
        return DB::table('p2ssixdigitnum')->insert(
            [
                'PTSSDNumDOC_NO' => $ptsSdNumDocNo,
                'TellerID' => $tellerId,
                'SixDigitNumber' => $sixDigitNumber,
                'SixDigitNumber2' => $sixDigitNumber2,
                'SixDigitWinPrize' => $sixDigitWinPrize,
                'SDDateOfDraw' => $sdDateOfDraw,
                'SDTotalCollection' => $sdTotalCollection,
                'SDDateTimeOfTrans' => $sdDateTimeTrans
            ]
        );
    }

    /** THREE DIGIT QUERIES */
    public static function getThreeDigitNumbers($term)
    {
        return DB::table('p2sthreedigitnum')
                    ->where('TDDateOfDraw', '=', $term)
                    ->orWhere('PTSTDNumDOC_NO', '=', $term)
                    ->get();
    }

    public static function getThreeDigit($ptsTdId)
    {
        return DB::table('p2sthreedigitnum')
                    ->where('PTSTDID', '=', $ptsTdId)
                    ->get();
    }

    public static function insertThreeDigit($ptsTdNumDocNo, $tellerId, $threeDigitNum, $threeDigitWinPrizeSolo, $threeDigitWinPrizeRumble, $tdDateOfDraw, $tdTotalCollection, $tdDateTimeTrans)
    {
        return DB::table('p2sthreedigitnum')->insert(
            [
                'PTSTDNumDOC_NO' => $ptsTdNumDocNo,
                'TellerID' => $tellerId,
                'ThreeDigitNum' => $threeDigitNum,
                'ThreeDigitWinPrizeSolo' => $threeDigitWinPrizeSolo,
                'threeDigitWinPrizeRumble' => $threeDigitWinPrizeRumble,
                'TDDateOfDraw' => $tdDateOfDraw,
                'TDTotalCollection' => $tdTotalCollection,
                'TDDateTimeTrans' => $tdDateTimeTrans
            ]
        );
    }

    /** SIX DIGIT WINNER QUERIES */
    // public static function getSixDigitWinners($ptsSdId)
    // {
    //     return DB::table('p2ssixdigitwinners as pd')
    //                 ->join('p2ssixdigitnum as pn', function($join){
    //                     $join->on('pd.PTSSDID', '=', 'pn.PTSSDID');
    //                 })
    //                 ->where('pd.PTSSDID', '=', $ptsSdId)
    //                 ->get();
    // }

    public static function insertSixDigitWinners($ptsSdWinBrCode, $ptsSdId, $docNumber, $tellerId, $clientAcctNum, $clientName, $winningNumber, $prizeWon, $dateOfWinning, $dateTimeOfTrans)
    {
        $exists = DB::table('p2ssixdigitwinners')
                    ->where('ClientAcctNum', '=', $clientAcctNum)
                    ->where('DateOfWinning', '=', $dateOfWinning)
                    ->where('DocNumber', '=', $docNumber)
                    ->where('WinningNumber', '=', $winningNumber)
                    ->first();
        
        if (!$exists) {
            return DB::table('p2ssixdigitwinners')->insert(
                [
                    'PTSDWinBR_CODE' => $ptsSdWinBrCode,
                    'PTSSDID' => $ptsSdId,
                    'DocNumber' => $docNumber,
                    'TellerID' => $tellerId,
                    'ClientAcctNum' => $clientAcctNum,
                    'ClientName' => $clientName,
                    'WinningNumber' => $winningNumber,
                    'PrizeWon' => $prizeWon,
                    'DateOfWinning' => $dateOfWinning,
                    'DateTimeOfTrans' => $dateTimeOfTrans
                ]
            );
        } else {
            return null;
        }
    }

    /** THREE DIGIT WINNER QUERIES */
    // public static function getThreeDigitWinners($ptsTdId)
    // {
    //     return DB::table('p2sthreedigitwinners as pd')
    //                 ->join('p2sthreedigitnum as pn', function($join){
    //                     $join->on('pd.PTSTDID', '=', 'pn.PTSTDID');
    //                 })
    //                 ->where('pd.PTSTDID', '=', $ptsTdId)
    //                 ->get();
    // }

    public static function insertThreeDigitWinners($ptsTdWinBrCode, $ptsTdId, $clientAcctNum, $clientName, $tellerId, $winningNumber, $prizeWon, $dateOfWinning, $dateTimeOfTrans, $ptsSerialId, $ptsStabsNumber, $ptsTdAdvance, $ptsTdNumOfEntries, $ptsTdRumbleSolo, $ptsTdTrDocNo, $ptsThreeDigitNum)
    {
        $exists = DB::table('p2sthreedigitwinners')
                    ->where('ClientAcctNum', '=', $clientAcctNum)
                    ->where('DateOfWinning', '=', $dateOfWinning)
                    ->where('PTSTDTRDOC_NO', '=', $ptsTdTrDocNo)
                    ->where('PTSThreeDigitNum', '=', $ptsThreeDigitNum)
                    ->first();
        
        if(!$exists) {
            return DB::table('p2sthreedigitwinners')->insert(
                [
                    'PTSTDWinBR_CODE' => $ptsTdWinBrCode,
                    'PTSTDID' => $ptsTdId,
                    'ClientAcctNum' => $clientAcctNum,
                    'ClientName' => $clientName,
                    'TellerID' => $tellerId,
                    'WinningNumber' => $winningNumber,
                    'PrizeWon' => $prizeWon,
                    'DateOfWinning' => $dateOfWinning,
                    'DateTimeOfTrans' => $dateTimeOfTrans,
                    'PTSSerialID' => $ptsSerialId,
                    'PTSStabsNumber' => $ptsStabsNumber,
                    'PTSTDAdvance' => $ptsTdAdvance,
                    'PTSTDNumOfEntries' => $ptsTdNumOfEntries,
                    'PTSTDRumbleSolo' => $ptsTdRumbleSolo,
                    'PTSTDTRDOC_NO' => $ptsTdTrDocNo,
                    'PTSThreeDigitNum' => $ptsThreeDigitNum
                ]
            );
        }
        else {
            return null;
        }
    }

    // public static function getAllWinnersByDate($date)
    // {
    //     return DB::table('p2sthreedigitwinners')
    //                 ->select(DB::raw('ClientAcctNum, ClientName, WinningNumber, TicketType, PrizeWon, DateOfWinning'))
    // }

    /** SIX DIGIT ENTRY */
    public static function getSixDigitEntry($clientId)
    {
        return DB::table('p2ssixdigitentries as psd')
                    ->join('client as c', function($join){
                        $join->on('psd.PTSSDClientID', '=', 'c.ClientID');
                    })
                    ->where('psd.PTSSDClientID', '=', $clientId)
                    ->orderBy('psd.PTSSDDateTrans', 'desc')
                    ->get();
    }

    /** SIX DIGIT WINNERS */
    public static function getAllSixDigitEntryWinners($sixdigit, $dateofdraw)
    {
        return DB::table('p2ssixdigitentries as psd')
                    ->join('client as c', function($join){
                        $join->on('psd.PTSSDClientID', '=', 'c.ClientID');
                    })
                    ->where('psd.PTSSDDateOfDraw', '>=', $dateofdraw.' 00:00:00')
                    ->where('psd.PTSSDDateOfDraw', '<', $dateofdraw.' 23:59:59')
                    ->where('psd.PTSSixDigitNum2', '=', $sixdigit)
                    ->get();
    }

    public static function getAllSixDigitByDate($date)
    {
        return DB::table('p2ssixdigitentries as psd')
                    ->join('client as c', function($join){
                        $join->on('psd.PTSSDClientID', '=', 'c.ClientID');
                    })
                    ->select(DB::raw('psd.PTSStabsNumber, c.ClientID, psd.PTSSDTRDOC_NO, psd.PTSSDDateOfTrans, psd.PTSSixDigitNum, psd.PTSSDDateTime'))
                    ->where('psd.PTSSDDateOfTrans', '=', $date)
                    ->get();
    }

    public static function getSixDigitByTicket($serialId, $branch, $serialNum)
    {
        return DB::table('p2ssixdigitentries as psd')
                    ->join('client as c', function($join){
                        $join->on('psd.PTSSDClientID', '=', 'c.ClientID');
                    })
                    ->where('psd.PTSSerialID', '=', $serialId)
                    ->where('psd.PTSSDEBR_CODE', '=', $branch)
                    ->where('psd.PTSStabsNumber', '=', $serialNum)
                    ->get();
    }

    public static function insertSixDigitEntry($ptsSdTrId, $ptsSdeBrCode, $tellerId, $ptsSerialId, $ptsStabsNumber, $ptsSdClientId, $ptsSdTrDocNo, $ptsSdNumOfEntries, $ptsSsdDateOfTrans, $ptsSixDigitNum, $ptsSixDigitNum2, $ptsSdDateTrans, $ptsSdDateOfDraw, $ptsSdAdvance, $ptsSdDateTime)
    {
        return DB::table('p2ssixdigitentries')->insert(
            [
                'PTSSDTR_ID' => $ptsSdTrId,
                'PTSSDEBR_CODE' => $ptsSdeBrCode,
                'TellerID' => $tellerId,
                'PTSSerialID' => $ptsSerialId,
                'PTSStabsNumber' => $ptsStabsNumber,
                'PTSSDClientID' => $ptsSdClientId,
                'PTSSDTRDOC_NO' => $ptsSdTrDocNo,
                'PTSSDNumOfEntries' => $ptsSdNumOfEntries,
                'PTSSDDateOfTrans' => $ptsSsdDateOfTrans,
                'PTSSixDigitNum' => $ptsSixDigitNum,
                'PTSSixDigitNum2' => $ptsSixDigitNum2,
                'PTSSDDateTrans' => $ptsSdDateTrans,
                'PTSSDDateOfDraw' => $ptsSdDateOfDraw,
                'PTSSDAdvance' => $ptsSdAdvance,
                'PTSSDDateTime' => $ptsSdDateTime,
            ]
        );
    }

    /** THREE DIGIT ENTRY */
    public static function getThreeDigitEntry($clientId)
    {
        return DB::table('p2sthreedigitentries as ptd')
                    ->join('client as c', function($join){
                        $join->on('ptd.PTSTDClientID', '=', 'c.ClientID');
                    })
                    ->where('ptd.PTSTDClientID', '=', $clientId)
                    ->orderBy('ptd.PTSTDDateTrans', 'desc')
                    ->get();
    }

    /** THREE DIGIT WINNERS */
    public static function getAllThreeDigitEntryWinners($threedigit, $dateofdraw, $numdoc)
    {
        $str = str_split($threedigit);
        $first = (string) $str[0] . (string) $str[1] . (string) $str[2];
        $second = (string) $str[0] . (string) $str[2] . (string) $str[1];
        $third = (string) $str[1] . (string) $str[2] . (string) $str[0];
        $fourth = (string) $str[1] . (string) $str[0] . (string) $str[2];
        $fifth = (string) $str[2] . (string) $str[0] . (string) $str[1];
        $sixth = (string) $str[2] . (string) $str[1] . (string) $str[0];

        return DB::table('p2sthreedigitentries as ptd')
                    ->join('client as c', function($join){
                        $join->on('ptd.PTSTDClientID', '=', 'c.ClientID');
                    })
                    ->where('ptd.PTSTDDateOfDraw', '>=', $dateofdraw.' 00:00:00')
                    ->where('ptd.PTSTDDateOfDraw', '<', $dateofdraw.' 23:59:59')
                    ->where('ptd.PTSThreeDigitNum', '=', $threedigit)
                    ->orWhere('ptd.PTSTDTRDOC_NO', '=', $numdoc)
                    ->orWhere(function($query) use ($first, $dateofdraw){
                        $query->where('ptd.PTSTDRumbleSolo', '=', 'rumble')
                            ->where('ptd.PTSTDDateOfDraw', '>=', $dateofdraw.' 00:00:00')
                            ->where('ptd.PTSTDDateOfDraw', '<', $dateofdraw.' 23:59:59')
                            ->where('ptd.PTSThreeDigitNum', '=', $first);
                    })
                    ->orWhere(function($query) use ($second, $dateofdraw){
                        $query->where('ptd.PTSTDRumbleSolo', '=', 'rumble')
                            ->where('ptd.PTSTDDateOfDraw', '>=', $dateofdraw.' 00:00:00')
                            ->where('ptd.PTSTDDateOfDraw', '<', $dateofdraw.' 23:59:59')
                            ->where('ptd.PTSThreeDigitNum', '=', $second);
                    })
                    ->orWhere(function($query) use ($third, $dateofdraw){
                        $query->where('ptd.PTSTDRumbleSolo', '=', 'rumble')
                            ->where('ptd.PTSTDDateOfDraw', '>=', $dateofdraw.' 00:00:00')
                            ->where('ptd.PTSTDDateOfDraw', '<', $dateofdraw.' 23:59:59')
                            ->where('ptd.PTSThreeDigitNum', '=', $third);
                    })
                    ->orWhere(function($query) use ($fourth, $dateofdraw){
                        $query->where('ptd.PTSTDRumbleSolo', '=', 'rumble')
                            ->where('ptd.PTSTDDateOfDraw', '>=', $dateofdraw.' 00:00:00')
                            ->where('ptd.PTSTDDateOfDraw', '<', $dateofdraw.' 23:59:59')
                            ->where('ptd.PTSThreeDigitNum', '=', $fourth);
                    })
                    ->orWhere(function($query) use ($fifth, $dateofdraw){
                        $query->where('ptd.PTSTDRumbleSolo', '=', 'rumble')
                            ->where('ptd.PTSTDDateOfDraw', '>=', $dateofdraw.' 00:00:00')
                            ->where('ptd.PTSTDDateOfDraw', '<', $dateofdraw.' 23:59:59')
                            ->where('ptd.PTSThreeDigitNum', '=', $fifth);
                    })
                    ->orWhere(function($query) use ($sixth, $dateofdraw){
                        $query->where('ptd.PTSTDRumbleSolo', '=', 'rumble')
                            ->where('ptd.PTSTDDateOfDraw', '>=', $dateofdraw.' 00:00:00')
                            ->where('ptd.PTSTDDateOfDraw', '<', $dateofdraw.' 23:59:59')
                            ->where('ptd.PTSThreeDigitNum', '=', $sixth);
                    })
                    ->get();
    }

    public static function getThreeDigitById($serialId, $branch, $serialNum)
    {
        return DB::table('p2sthreedigitentries as ptd')
                    ->join('client as c', function($join){
                        $join->on('ptd.PTSTDClientID', '=', 'c.ClientID');
                    })
                    ->where('ptd.PTSSerialID', '=', $serialId)
                    ->where('ptd.PTSTDEBR_CODE', '=', $branch)
                    ->where('ptd.PTSStabsNumber', '=', $serialNum)
                    ->get();
    }

    public static function getAllThreeDigitByDate($date)
    {
        return DB::table('p2sthreedigitentries as ptd')
                    ->join('client as c', function($join){
                        $join->on('ptd.PTSTDClientID', '=', 'c.ClientID');
                    })
                    ->select(DB::raw('ptd.PTSStabsNumber, c.ClientID, ptd.PTSTDTRDOC_NO, ptd.PTSTDDateOfTrans, ptd.PTSThreeDigitNum, ptd.PTSTDRumbleSolo, ptd.PTSTDDateTime'))
                    ->where('ptd.PTSTDDateOfTrans', '=', $date)
                    ->get();
    }

    public static function insertThreeDigitEntry($ptsTdTrId, $ptsTdeBrCode, $tellerId, $ptsSerialId, $ptsStabsNumber, $ptsTdClientId, $ptsTdTrDocNo, $ptsTdNumOfEntries, $ptsTdDateOfTrans, $ptsThreeDigitNum,
    $ptsTdRumbleSolo, $ptsTdDateTrans, $ptsTdDateofDraw, $ptsTdAdvance, $ptsTdDatetime)
    {
        return DB::table('p2sthreedigitentries')->insert(
            [
                'PTSTDTR_ID' => $ptsTdTrId,
                'PTSTDEBR_CODE' => $ptsTdeBrCode,
                'TellerID' => $tellerId,
                'PTSSerialID' => $ptsSerialId,
                'PTSStabsNumber' => $ptsStabsNumber,
                'PTSTDClientID' => $ptsTdClientId,
                'PTSTDTRDOC_NO' => $ptsTdTrDocNo,
                'PTSTDNumOfEntries' => $ptsTdNumOfEntries,
                'PTSTDDateOfTrans' => $ptsTdDateOfTrans,
                'PTSThreeDigitNum' => $ptsThreeDigitNum,
                'PTSTDRumbleSolo' => $ptsTdRumbleSolo,
                'PTSTDDateTrans' => $ptsTdDateTrans,
                'PTSTDDateOfDraw' => $ptsTdDateofDraw,
                'PTSTDAdvance' => $ptsTdAdvance,
                'PTSTDDateTime' => $ptsTdDatetime,
            ]
        );
    }

    /** LIST OF CLIENTS WHO PAY FOR PLAY TO SAVE */
    public static function getAllListOfClient() 
    {
        return DB::table('transsum as tr')
                    ->join('client as c', function($join){
                        $join->on('tr.TR_CLIENTID', '=', 'c.ClientID');
                    })
                    ->get();
    }

    /** LIST OF WINNERS FOR THE DAY */
    public static function getAllWinnersForThreeDigitDate($date) 
    {
        return DB::table('p2sthreedigitwinners as ptw')
                    ->join('client as c', function($join){
                        $join->on('ptw.ClientAcctNum', '=', 'c.ClientID');
                    })
                    ->select(DB::raw('ptw.PTSTDWinBR_CODE, ptw.PTSSerialID, ptw.ClientAcctNum, c.FName, c.LName, c.MName, c.SName, ptw.PTSThreeDigitNum, ptw.WinningNumber, ptw.PTSTDRumbleSolo, ptw.PrizeWon, ptw.DateOfWinning'))
                    ->where('ptw.DateOfWinning', '=', $date)
                    ->get();
    }

    public static function getAllWinnersForThreeDigitBranch($branch) 
    {
        return DB::table('p2sthreedigitwinners as ptw')
                    ->join('client as c', function($join){
                        $join->on('ptw.ClientAcctNum', '=', 'c.ClientID');
                    })
                    ->select(DB::raw('ptw.PTSTDWinBR_CODE, ptw.PTSSerialID, ptw.ClientAcctNum, c.FName, c.LName, c.MName, c.SName, ptw.PTSThreeDigitNum, ptw.WinningNumber, ptw.PTSTDRumbleSolo, ptw.PrizeWon, ptw.DateOfWinning'))
                    ->where('ptw.PTSTDWinBR_CODE', '=', $branch)
                    ->get();
    }

    public static function getAllWinnersForSixDigitDate($date)
    {
        return DB::table('p2ssixdigitwinners as psw')
                    ->join('client as c', function($join){
                        $join->on('psw.ClientAcctNum', '=', 'c.ClientID');                        
                    })
                    ->select(DB::raw('psw.PTSDWinBR_CODE, psw.PTSSDID, psw.ClientAcctNum, c.FName, c.LName, c.MName, c.SName, psw.WinningNumber, psw.PrizeWon, psw.DateOfWinning'))
                    ->where('psw.DateOfWinning', '=', $date)
                    ->get();
    }

    public static function getAllWinnersForSixDigitBranch($branch)
    {
        return DB::table('p2ssixdigitwinners as psw')
                    ->join('client as c', function($join){
                        $join->on('psw.ClientAcctNum', '=', 'c.ClientID');                        
                    })
                    ->select(DB::raw('psw.PTSDWinBR_CODE, psw.PTSSDID, psw.ClientAcctNum, c.FName, c.LName, c.MName, c.SName, psw.WinningNumber, psw.PrizeWon, psw.DateOfWinning'))
                    ->where('psw.PTSDWinBR_CODE', '=', $branch)
                    ->get();
    }
}
